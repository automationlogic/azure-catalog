#!/bin/bash
echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ wheezy main" | sudo tee /etc/apt/sources.list.d/azure-cli.list
curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -
apt-get update && apt-get -y install nodejs apache2 jq apt-transport-https azure-cli --allow-unauthenticated
